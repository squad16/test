#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

from setuptools import find_packages, setup, Command

# Package meta-data.
NAME = 'valid_test_example'
DESCRIPTION = 'Example Test'
URL = 'https://gitlab.com/squad16/test'
EMAIL = 'rostislav.izimov@glowbyteconsulting.com'
AUTHOR = 'R. Izimov'
REQUIRES_PYTHON = '>=3.0.0'
VERSION = '0.0.0'

REQUIRED = [
    'requests',
    'numpy'
]

setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    author=AUTHOR,
    author_email=EMAIL,
    #python_requires=REQUIRES_PYTHON,
    url=URL,
    packages = find_packages(),
    # If your package is a single module, use this instead of 'packages':
    # py_modules=['mypackage'],
    install_requires=REQUIRED,
    # extras_require=EXTRAS,
    include_package_data=True,
    classifiers=[],
    entry_points={}
    # $ setup.py publish support.
    #cmdclass={
    #    'upload': UploadCommand,
    #},
)